package blob

import (
	"bytes"
	"crypto/sha256"
	"fmt"
)

type sha224Digest [28]byte

// GetRef returns the reference (sha224) of the blob.
func GetRef(data *[]byte) string {
	var h sha224Digest = sha256.Sum224(*data)
	return h.digestRef()
}

func GetReader(data []byte) *bytes.Reader {
	return bytes.NewReader(data)
}

func (d sha224Digest) digestRef() string {
	return fmt.Sprintf("%s-%02x", d.digestName(), d)
}

func (d sha224Digest) digestName() string { return "sha224" }
